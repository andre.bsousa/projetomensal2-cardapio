import React from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'
import styled from 'styled-components'
import { GiNinjaHeroicStance } from 'react-icons/gi'
import { NavLink } from 'react-router-dom'

export default () => {

    const menu = [
    {
        name: "Home",
        link: '/',
        icon: ''
    },

    {
        name: "Sobre",
        link: 'sobre',
        icon: ''
    },

    {
        name: "Produtos",
        link: 'produtos',
        icon: ''
    },

    {
        name: "Serviços",
        link: 'servicos',
        icon: ''
    },

    {
        name: "Contato",
        link: 'contato',
        icon: ''
    }
]



    return (
        <div>
            <Header>
                <Container>
                    <Navbar variant="dark" expand="lg">
                        <Navbar.Brand href="#home">
                        <Logo>
                        <GiNinjaHeroicStance />Xablau
                        <br />
                        <span>Burguer</span>
                        </Logo>
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
                            <Nav>
                            {menu.map((item, i) => (
                                <NavLink key={i} to={item.link}>
                                <Nav.Link as="div">{item.name}</Nav.Link>
                                </NavLink>                            
                            ))  }
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </Container>

            </Header>

        </div>
    )
}

const Header = styled.div`
background: black;
.nav-link:hover{
    color: red !important;
    font-weight: 500;
}
`

const Logo = styled.div`
font-size: 20px;
font-weight: 600;
margin: 0;
font-family: 'Josefin Sans', sans-serif;
svg{
    color: #eee;
    margin: -5px 1px;

}
span{
    color: #eee;
    margin: 0;
    font-size: 12px;
    text-transform: uppercase;
    display: block;
    text-align: center;
    letter-spacing: 4px;
}
`
