import React from 'react'
import Header from './header'
import Footer from './footer'
import styled from 'styled-components'

const Layout = ({ children }) => {

    return (
        <>
            <HeadContainer>
            <Header />
            </HeadContainer>
            <Content>
                {children}
            </Content>
            <FooterContainer>
            <Footer />

            </FooterContainer>
        </>
    )
}

export default Layout

const HeadContainer = styled.div`
`

const Content = styled.div`
background: orange;
min-height: 500px;
`

const FooterContainer = styled.div`
background: green;
height: 200px;
width: 100%
`