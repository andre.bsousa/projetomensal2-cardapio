import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'
import { AiFillFacebook, AiFillTwitterCircle, AiFillInstagram } from 'react-icons/ai'

export default () => {
    return (
        <>
        <Footer>

            <Container>
                <FooterInfo>
                    <Row>
                        <Col md={5}>
                            <div className="title">
                            Sobre Nós
                            </div>
                            <div className="about">
                            <p>s simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry's standard dummy text ever 
                            since the 1500s, when an unknown printer took a galley of type and scrambled i</p>
                            </div>                                
                        </Col>
                        <Col md={4}>
                            <div className="title">
                                As mais pedidas
                            </div>
                            <div className="menu"></div>
                        </Col>
                        <Col md={3}>
                            <div className="title">
                                Onde estamos
                            </div>
                            <div className="address"></div>
                        </Col>
                    </Row>
                </FooterInfo>
                <Row>
                    <FooterSocial>
                        <AiFillFacebook />
                        <AiFillInstagram />
                        <AiFillTwitterCircle />
                    </FooterSocial>
                    <FooterCopy>
                        All rights reserved
                    </FooterCopy>

                </Row>
            </Container>
        </Footer>






        </>
    )
}

const Footer = styled.div`
background: #111;
padding: 10px auto;
color: gray;
border-top: 1px solid #eee;
`

const FooterInfo = styled.div`
.title{
    font-size: 20px;
    font-weight: 600;
    padding: 5px 0;
    border-bottom: thin solid #eee;
    color: #eee;
    margin-bottom: 10px;
}
`
const FooterCopy = styled.div`
width: 100%;
paddin: 5px;
text-align: center;
`
const FooterSocial = styled.div`
width: 100%;
border-top: 1px dotted #ccc;
padding: 5px;
svg {
    cursor: pointer;
    margin: 5px;
    font-size: 30px;
    &:hover{
        color: red;
    }
}
` 