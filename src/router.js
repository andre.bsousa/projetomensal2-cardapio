import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import Portal from './views/portal';
import history from './config/history'
import AdminDash from './views/admin'




const Routers = () => {

    return (

    <Router history={history}>
        <Switch>
            <Route path="/admin" component={AdminDash}></Route>
            <Route path="/" component={Portal}></Route>
        </Switch>
    </Router> 
    )   
}

export default Routers