import React from 'react'
import Home from './home'
import About from './about'
import Layout from '../../components/layout'
import { Route } from 'react-router-dom'
import about from './about'

export default ({match}) => {

    const Menu = [{
        Home:{
            name: 'Home',
            basename: match.path,
            path: match.path + '/',
            component: Home
        },
        
        Product:{
            name: 'Produtos',
            basename: match.path,
            path: match.path + 'produtos',
            component: () => <h1>Produtos</h1>
        },

        About: {
            name: 'About',
            basename: match.path,
            path: match.path + 'sobre',
            component: () => <h1>sobre</h1>
        },

        Services: {
            name: 'Serviços',
            basename: match.path,
            path: match.path + 'servicos',
            component: () => <h1>Serviços</h1>
        },

        Contact: {
            name: 'Contato',
            basename: match.path,
            path: match.path + 'contato',
            component: () => <h1>contato</h1>
        }   


    }]


    return (
        <Layout>
        { /*Menu.map((item, i) => (
            <Route exact basename={match.path} path={match.path + item.path} component={item.component} key={i} />
        ))*/}
        <Route exact basename={match.path} path={match.path} component={Home} />
        <Route exact basename={match.path} path={match.path + 'sobre'} component={() => <h1>Sobre</h1>} />
        <Route exact basename={match.path} path={match.path + 'produtos'} component={() => <h1>Produtos</h1>} />
        <Route exact basename={match.path} path={match.path + 'servicos'} component={() => <h1>Serviços</h1>} />
        <Route exact basename={match.path} path={match.path + 'contato'} component={() => <h1>Contato</h1>} />           
        </Layout>
    )
}

