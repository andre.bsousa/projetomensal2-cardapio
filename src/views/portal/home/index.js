import React from 'react';
import styled from 'styled-components'
import { BiHome } from 'react-icons/bi'
import About from './about'
import Banner from './banner'
import Menu from './menu'
import Products from './products'
import Info from './info'



const Home = () => {
    return (
        <>
        <HomeContainer>
        <About />
        <Banner />
        <Menu />
        <Products />
        <Info />
        </HomeContainer>
        </>
    )
}

export default Home

const HomeContainer = styled.div`
background: gray;
`

const IconHome = styled(BiHome)`
color: crimson;
`