import React from 'react'
import Dash from './dash'
import { Route } from 'react-router-dom'

export default ({match}) => {
    return (
        <>
        <Route exact basename={match.path} path={match.path + '/'} component={Dash} />
        <Route exact basename={match.path} path={match.path + '/services'} component={() => <h1>Serviços foda-se</h1>} /> 
        </>           
            
    )
}

