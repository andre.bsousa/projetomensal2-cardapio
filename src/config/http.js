import axios from 'axios'

const localhost = `http://localhost:3000`

const http = axios.create({
    baseURL: process.env.REACT_APP_API || localhost    
})

http.defaults.headers['Content-type'] = 'application/json'

export {
    http
}